package com.restaurantApp.exceptions;

@SuppressWarnings("serial")
public class LockedOrderException extends OrderException {
	
	private static final String MESSAGE = "Can't add item to an order that is locked.";
	
	public LockedOrderException() {
		super(MESSAGE);
	}

}
