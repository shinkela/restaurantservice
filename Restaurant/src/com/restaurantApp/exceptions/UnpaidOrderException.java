package com.restaurantApp.exceptions;

@SuppressWarnings("serial")
public class UnpaidOrderException extends OrderException {
	
	private static final String MESSAGE =  "Can't create new order for table if unpaid order exists.";
	
	public UnpaidOrderException() {
		super(MESSAGE);
	}

}
