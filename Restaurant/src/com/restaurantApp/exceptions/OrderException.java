package com.restaurantApp.exceptions;

@SuppressWarnings("serial")
public class OrderException extends RuntimeException {
	
	private static final String MESSAGE = "Order can't be placed";
	
	public OrderException() {
		super(MESSAGE);
	}
	
	public OrderException(String message) {
		super(message);
	}
	

}
