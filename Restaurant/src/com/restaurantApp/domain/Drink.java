package com.restaurantApp.domain;

import com.restaurantApp.utility.RestoranUtility;

public class Drink implements Orderable {

	private String name;
	private double price;
	private int volumeInMililiters;
	private DrinkType drinkType;

	public Drink(String name,int volumeInMililiters, DrinkType drinkType) {
		super();
		this.name = name;
		this.price = RestoranUtility.generatePrice(150.00, 500.00);
		this.volumeInMililiters = volumeInMililiters;
		this.drinkType = drinkType;
	}

	@Override
	public double getPrice() {
		return price;
	}

}
