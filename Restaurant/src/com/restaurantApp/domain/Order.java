package com.restaurantApp.domain;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.restaurantApp.exceptions.LockedOrderException;

public class Order {

	private static long idCounter = 0;

	private long id;
	private List<Orderable> orderedItems;
	private Table table;
	private boolean isPayed;
	private boolean isLocked;

	public Order(Table table) {
		this.id = ++idCounter;
		this.table = table;
		this.isPayed = false;
		orderedItems = new ArrayList<Orderable>();
		System.out.println("Order: " + LocalDate.now() + " " + "table: " +  table.getTableNumber() );
	}

	public void addItemToOrder(Orderable orderable) throws LockedOrderException {
		
		if(isLocked) {			
			throw new LockedOrderException();
		}
		
		orderedItems.add(orderable);

	}

	public double getTotalPrice() {

		double retVal = 0;

		for (Orderable orderable : orderedItems) {
			retVal += orderable.getPrice();
		}

		return retVal;
	}


	public void pay() {
		this.isPayed = true;

	}

	public boolean isLocked() {
		return isLocked;
	}

	public void lock() {
		this.isLocked = true;
	}
	
	public boolean isPayed() {
		return isPayed;
	}
	
	public long getTableNumber() {
		
		return table.getTableNumber();
	}


}
