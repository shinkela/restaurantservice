package com.restaurantApp.domain;

public enum FoodType {
	PIZZA, PASTA
}
