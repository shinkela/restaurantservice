package com.restaurantApp.domain;

public interface Orderable {
	
	public double getPrice();

}
