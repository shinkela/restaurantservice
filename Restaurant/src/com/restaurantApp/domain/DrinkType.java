package com.restaurantApp.domain;

public enum DrinkType {
	
	SODA, JUICE, WATER

}
