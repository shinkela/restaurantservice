package com.restaurantApp.domain;

import java.util.ArrayList;
import java.util.List;

import com.restaurantApp.exceptions.UnpaidOrderException;

public class Table {

	private List<Order> payedOrders;
	private Long tableNumber;
	private Order newestOrder;

	public Table(long tableNumber) {
		this.tableNumber = tableNumber;
		payedOrders = new ArrayList<Order>();
	}

	public Order createOrder() throws UnpaidOrderException {

		if (newestOrder != null && !newestOrder.isPayed()) {
			throw new UnpaidOrderException();
		} else {

			newestOrder = new Order(this);
		}

		return newestOrder;
	}

	public void lockOrder(Order order) {
		order.lock();

	}

	public Receipt chargeAnOrder(Order order) {
		order.pay();
		payedOrders.add(order);

		return new Receipt(order);
	}

	public Long getTableNumber() {
		return tableNumber;
	}
	
	

}
