package com.restaurantApp.domain;

import com.restaurantApp.utility.RestoranUtility;

public class Addition implements Orderable {

	private String name;
	private double price;

	public Addition(String name) {
		this.name = name;
		this.price = RestoranUtility.generatePrice(20.00, 120.00);
	}

	public String getName() {
		return name;
	}
	
	@Override
	public double getPrice() {
		return price;
	}

}
