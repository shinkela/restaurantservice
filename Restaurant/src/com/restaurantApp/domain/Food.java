package com.restaurantApp.domain;

import com.restaurantApp.utility.RestoranUtility;

public class Food {

	private String name;
	private double price;
	private FoodType foodType;

	public Food(String name, FoodType foodType) {
		super();
		this.name = name;
		this.price = RestoranUtility.generatePrice(300.00, 600.00);
		this.foodType = foodType;

	}

	public double getPrice() {

		return this.price;
	}

}
