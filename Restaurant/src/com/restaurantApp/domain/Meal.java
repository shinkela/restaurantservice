package com.restaurantApp.domain;

import java.util.ArrayList;
import java.util.List;

public class Meal implements Orderable {
	
	private static long idCounter = 0;
	
	private long id;
	private Food food;
	private List<Addition> additions;
	
	public Meal(Food food) {
		this.id = ++idCounter;
		this.food = food;
		this.additions = new ArrayList<Addition>();
	}

	@Override
	public double getPrice() {
		double retVal = food.getPrice();
		for (Addition addition : additions) {
			retVal += addition.getPrice();
		}
		return retVal;
	}
	
	public void addAddition(Addition addition) {
		additions.add(addition);
	}

}
