package com.restaurantApp.domain;

import java.util.HashMap;
import java.util.Map;

public class Restaurant {
	
	private final String name = "Djema Gondola - Italian Restaurant";
	private Map<Long, Table> tables = new HashMap<Long, Table>();
	private static Restaurant instanceOf = null;
	
	private Restaurant() {
		for (int i = 1; i < 5; i++) {
			tables.put(Long.valueOf(i), new Table(i));
		}
	}
	
	public static Restaurant getInstanceOf() {
		if(instanceOf == null) {
			instanceOf = new Restaurant();
		}
		
		return instanceOf;

	}
	
	public Table getTable(long tableNum) {
		
		return tables.get(tableNum);
	}
	
	
	

}
