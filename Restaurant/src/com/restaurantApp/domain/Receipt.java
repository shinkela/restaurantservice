package com.restaurantApp.domain;

import java.time.LocalDate;

public class Receipt {

	private Order order;
	private double totalPrice;

	public Receipt(Order order) {
		this.order = order;
		this.totalPrice = order.getTotalPrice();
		System.out.println(
				"Receipt: " + LocalDate.now() + " " + "table: " + order.getTableNumber()
				+ " total amount: " + totalPrice );
	}
	
	

}
