package com.restaurantApp;

import java.util.List;

import com.restaurantApp.domain.Addition;
import com.restaurantApp.domain.Drink;
import com.restaurantApp.domain.DrinkType;
import com.restaurantApp.domain.Food;
import com.restaurantApp.domain.FoodType;
import com.restaurantApp.domain.Meal;
import com.restaurantApp.domain.Order;
import com.restaurantApp.domain.Receipt;
import com.restaurantApp.domain.Restaurant;
import com.restaurantApp.domain.Table;
import com.restaurantApp.utility.RestoranUtility;

public class AppMain {



	public static void main(String[] args) {

		Restaurant restaurant = Restaurant.getInstanceOf();
		Table table1 = restaurant.getTable(1);
		Table table2 = restaurant.getTable(2);
		Table table3 = restaurant.getTable(3);
		
		List<Food> pizzas = RestoranUtility.generateFood(4, FoodType.PIZZA);
		List<Food> pastas = RestoranUtility.generateFood(5, FoodType.PASTA);
		List<Addition> additions = RestoranUtility.generateAdditions(5);
		Drink drink1 = new Drink("Coca Cola",330, DrinkType.SODA);
		Drink drink2 = new Drink("Next", 250, DrinkType.JUICE);
		Drink drink3 = new Drink("Prolom", 330, DrinkType.WATER);
		
		
		//ORDER 1
		Order order1 = table1.createOrder();
		Meal meal1 = new Meal(pizzas.get(1));
		meal1.addAddition(additions.get(1));
		meal1.addAddition(additions.get(2));
		Meal meal2 = new Meal(pastas.get(1));
		meal2.addAddition(additions.get(3));
		order1.addItemToOrder(meal1);
		order1.addItemToOrder(meal2);
		order1.addItemToOrder(drink1);
		order1.addItemToOrder(drink1);
		table1.lockOrder(order1);
		
		//ORDER 2
		Order order2 = table2.createOrder();
		Meal meal3 = new Meal(pizzas.get(2));
		Meal meal4 = new Meal(pastas.get(1));
		order2.addItemToOrder(meal3);
		order2.addItemToOrder(meal4);
		order2.addItemToOrder(drink2);
		table2.lockOrder(order2);
		
		//ORDER 3
		Order order3 = table3.createOrder();
		Meal meal5 = new Meal(pizzas.get(1));
		Meal meal6 = new Meal(pizzas.get(1));
		Meal meal7 = new Meal(pizzas.get(1));
		
		order3.addItemToOrder(meal5);
		order3.addItemToOrder(meal6);
		order3.addItemToOrder(meal7);
		order3.addItemToOrder(additions.get(1));
		order3.addItemToOrder(additions.get(1));
		
		order3.addItemToOrder(drink1);
		order3.addItemToOrder(drink2);
		order3.addItemToOrder(drink3);
		
		table3.lockOrder(order3);
		
		// First and third order charged
		Receipt receipt1 =  table1.chargeAnOrder(order1);
		Receipt receipt3 =  table3.chargeAnOrder(order3);
		
		// Tried to create new order for table 2, withou paying the last order.
		try {
			
			Order order4 = table2.createOrder();
			Meal meal8 = new Meal(pizzas.get(1));
			order4.addItemToOrder(meal8);
			table2.lockOrder(order4);
			
			
			
		} catch (Exception e) {
			System.out.println("EXCEPTION: " + e.getMessage());
		}
		
		Receipt receipt2  = table2.chargeAnOrder(order2);
		
		// Order succesful after paying the last order. 
		try {
			
			Order order4 = table2.createOrder();
			Meal meal8 = new Meal(pizzas.get(1));
			order4.addItemToOrder(meal8);
			table2.lockOrder(order4);
			
			
			
		} catch (Exception e) {
			System.out.println("EXCEPTION: " + e.getMessage());
		}
	





	}

}
