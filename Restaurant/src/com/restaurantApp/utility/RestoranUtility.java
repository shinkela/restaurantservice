package com.restaurantApp.utility;

import java.util.ArrayList;
import java.util.List;

import com.restaurantApp.domain.Addition;
import com.restaurantApp.domain.Drink;
import com.restaurantApp.domain.DrinkType;
import com.restaurantApp.domain.Food;
import com.restaurantApp.domain.FoodType;

public class RestoranUtility {

	public static double generatePrice(double min, double max) {
		double price = Math.random() * (max - min) + min;
		price = Math.round(price);
		return price;
	}

	public static List<Food> generateFood(int numOfFoodObjects, FoodType foodType) {
		List<Food> food = new ArrayList<Food>();
		for (int i = 0; i < numOfFoodObjects; i++) {
			food.add(new Food(foodType.toString() + " " + i, foodType));
		}
		return food;
	}

	public static List<Drink> generateDrinks(int numOfDrinkObjects, DrinkType drinkType) {

		List<Drink> drinks = new ArrayList<Drink>();
		for (int i = 0; i < numOfDrinkObjects; i++) {
			drinks.add(new Drink(drinkType.toString() + " " + i, 330, drinkType));
		}

		return drinks;
	}

	public static List<Addition> generateAdditions(int numOfAdditions) {

		List<Addition> additions = new ArrayList<>();
		for (int i = 0; i < numOfAdditions; i++) {
			additions.add(new Addition("addition " + i));
		}

		return additions;
	}

}
